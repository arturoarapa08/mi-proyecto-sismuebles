<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Categorias extends CI_Controller {

    public function __construct(){
    parent::__construct();
    $this->load->model("Categorias_model");
}

	public function index()
	{
		$data = array(
            'categorias' => $this->Categorias_model->getCategorias(),
        );  
        $this->load->view('layouts/header');
		$this->load->view('layouts/aside');
		$this->load->view('admin/categorias/list',$data);
		$this->load->view('layouts/footer');

	}
    public function add(){
        
            $this->load->view('layouts/header');
            $this->load->view('layouts/aside');
            $this->load->view('admin/categorias/add');
            $this->load->view('layouts/footer');       
    }
    public function store(){
        $nombre = $this->input->post("nombre");
        $descripcion = $this->input->post("descripcion");
        
        $this->form_validation->set_rules("nombre","Nombre","required|is_unique[categorias.nombre]");          
        if ($this->form_validation->run()) {
            
            $data = array(
            'nombre' => $nombre,
            'descripcion' => $descripcion,
            'estado' => "1" 
            
            );

        if($this->Categorias_model->save($data)){
            redirect(base_url()."mantenimiento/categorias");
        }
        else{
            $this->session->set_flashdata("error","No se pudo guardar la informacion");
            redirect(base_url()."mantenimiento/categorias/add");
        }
        }
        else{
            $this->add();
        }


    }

    public function edit($id){

            $data = array(
                'categoria' => $this->Categorias_model->getCategoria($id) ,
                );
            $this->load->view('layouts/header');
            $this->load->view('layouts/aside');
            $this->load->view('admin/categorias/edit',$data);
            $this->load->view('layouts/footer');
    }
    public function update(){
        $idCategoria = $this->input->post("idCategoria");
        $nombre = $this->input->post("nombre");
        $descripcion = $this->input->post("descripcion");

        $categoriaActual = $this->Categorias_model->getCategoria($idCategoria);

        if ($nombre == $categoriaActual->nombre) {
            $unique = '';
        }
        else{
            $unique = '|is_unique[categorias.nombre]';
        }
        

        $this->form_validation->set_rules("nombre","Nombre","required".$unique);          
        if ($this->form_validation->run()) {

            $data = array(
            'nombre' => $nombre,
            'descripcion' => $descripcion,
        );
        if($this->Categorias_model->update($idCategoria,$data)){
            redirect(base_url()."mantenimiento/categorias");
        }
        else{
            $this->session->set_flashdata("error","No se pudo actualizar la informacion");
            redirect(base_url()."mantenimiento/categorias/edit/".$idCategoria);
        }

        }
        else{
                $this->edit($idCategoria);
        }
        
        

    }
    public function view($id){
        $data = array(
            'categoria' => $this->Categorias_model->getCategoria($id), 
            );
            $this->load->view("admin/categorias/view",$data);


    }
    public function delete($id){
        $data = array(
            'estado' => "0",
            );
            $this->Categorias_model->update($id, $data);
            echo "mantenimiento/categorias";

    }


       public function listacategoriasxls()
    {

      //Cargamos la librería de excel.
      $this->load->library('excel');
      $lista=$this->categorias_model->listacategorias();
      $lista=$lista->result();

      $this->excel->setActiveSheetIndex(0);


      $nombrehoja="listacategorias";
      $nombrearchivo=$nombrehoja.".xls";

      //metadatos del archivo
      $this->excel->getProperties()->setCreator("Juan Creador")
                             ->setLastModifiedBy("Juan Modificador")
                             ->setTitle("Titulo")
                             ->setSubject("Asunto")
                             ->setDescription("Documento de prueba")
                             ->setKeywords("Excel PHP")
                             ->setCategory("Categoria del documento");

        $this->excel->getActiveSheet()->setTitle($nombrehoja);
        //Contador de filas
        $contador = 1;
        
        //tamano texto
        $this->excel->getDefaultStyle()->getFont()->setName('Arial')->setSize(15);

        //fondo celdas
        $this->excel->getActiveSheet()->getStyle('A1:C1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID); 
        $this->excel->getActiveSheet()->getStyle('A1:C1')->getFill()->getStartColor()->setARGB('cdcdcd'); 

        //Le aplicamos ancho las columnas.
        $this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(5);
        $this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
        $this->excel->getActiveSheet()->getColumnDimension('C')->setWidth(20);
        //si queremos que el texto se ajuste
        //$this->excel->getActiveSheet()->getStyle('A1')->getAlignment()->setWrapText(true);
        //si queremos que el el tamano sea automatico
        //$this->excel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);

        //Le aplicamos negrita a los títulos de la cabecera.
        $this->excel->getActiveSheet()->getStyle("A{$contador}")->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle("B{$contador}")->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle("C{$contador}")->getFont()->setBold(true);
        //Definimos los títulos de la cabecera.
        $this->excel->getActiveSheet()->setCellValue("A{$contador}", '#.');
        $this->excel->getActiveSheet()->setCellValue("B{$contador}", 'nombre');
        $this->excel->getActiveSheet()->setCellValue("C{$contador}", 'descripcion');
        //Definimos la data del cuerpo.    

        //tamano texto y bold
        $this->excel->getDefaultStyle()->getFont()->setName('Arial')->setSize(10);
        $this->excel->getActiveSheet()->getStyle()->getFont()->setBold(false);

        foreach($lista as $row){
           //Incrementamos una fila más, para ir a la siguiente.
           $contador++;
           //Informacion de las filas de la consulta.
           $this->excel->getActiveSheet()->setCellValue("A{$contador}", $contador-1);
           $this->excel->getActiveSheet()->setCellValue("B{$contador}", $row->nombre);
           $this->excel->getActiveSheet()->setCellValue("C{$contador}", $row->descripcion);
        }

        //Le ponemos un nombre al archivo que se va a generar.
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="'.$nombrearchivo.'"');
        header('Cache-Control: max-age=0');
        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
        //Hacemos una salida al navegador con el archivo Excel.
        $objWriter->save('php://output');
}


}
