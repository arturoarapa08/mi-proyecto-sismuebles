  <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <h1>
                Clientes
                <small>Nuevo</small>
                </h1>
            </section>
            <!-- Main content -->
            <section class="content">
                <!-- Default box -->
                <div class="box box-solid">
                    <div class="box-body">
                    
                    <hr>
                    <div class="row">
                    <div class="col-md-12">

                    <?php if($this->session->flashdata("error")):?>
                    <div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <p><i class="icon fa fa-ban"></i> <?php echo $this->session->flashdata("error");?></p>
                    danger
                    </div>
                    <?php endif;?>



                    <form action="<?php echo base_url();?>mantenimiento/clientes/store" method="POST">
                    <div class="form-group">
                    <label for="nombres">Nombres:</label>
                    <input type="text" class="form-control" pattern="[A-Za-z0-9 ]{2,40}"
         title="Letras y números. Tamaño mínimo: 2. Tamaño máximo: 40" placeholder="Nombres" id="nombres" name="nombres"  >
                    </div>

                    <div class="form-group">
                    <label for="apellidos">Apellidos:</label>
                    <input type="text" class="form-control" id="apellidos"  placeholder="Apellidos" pattern="[A-Za-z0-9 ]{2,40}"
         title="Letras y números. Tamaño mínimo: 2. Tamaño máximo: 40" name="apellidos">
                    </div>

                    <div class="form-group">
                    <label for="telefono">Telefono:</label>
                    <input type="text" class="form-control" placeholder="telefono" id="telefono" name="telefono">
                    </div>


                    <div class="form-group">
                    <label for="direccion">Direccion:</label>
                    <input type="text" class="form-control" placeholder="Direccion" id="direccion"  name="direccion">
                    </div>

                    <div class="form-group">
                    <label for="ruc">NIT/CI:</label>
                    <input type="text" class="form-control" placeholder="NIT/CI" id="ruc" name="ruc">
                    </div>

                    <div class="form-group">
                    <label for="empresa">Empresa:</label>
                    <input type="text" class="form-control" placeholder="Empresa" id="empresa" name="empresa">
                    </div>

                    <div class="form-group">
                    <button type="submit" class="btn btn-success btn-flat">Guardar</button>
                    </div>
                    
                    </form>
                    </div>
                    </div>
                    </hr>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </section>
            <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->